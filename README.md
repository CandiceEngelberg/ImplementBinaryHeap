# C++ Program to Implement Binary Heap
==================
Javascript Implementation of heap and heap sort

A heap is a way to organize the elements of a range that allows for fast retrieval of the element with the highest value at any moment.
This code is simplest & easily can be understand & fastly implemented.
Binary function that accepts two elements in the range as arguments, and returns a value convertible to bool. 
This can either be a function pointer or a function object.

Candice Engelberg - Software developer at http://www.washingtontractor.com/
